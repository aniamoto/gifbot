class CreateMentions < ActiveRecord::Migration
  def change
    create_table :mentions do |t|
      t.bigint :tweet_id, null: false
      t.timestamps null: false
    end
  end
end
