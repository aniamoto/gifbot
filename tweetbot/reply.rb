class Reply
  def self.generate_reply_to(tweet)
    num, gif = 1, nil  #words[0] is the @username
  	words = tweet.text.split(" ")
  	while num < words.length && !gif
  	  tag = words[num].gsub(/[\W]/i, '')
  	  if tag.empty?
        num += 1
      else
        gif = Gif.get(tag)
      end
    end
  	  gif || "please talk to me in English"
  end

  def self.check_mentions
  	mentions = Mention.all
    last_mention_id = mentions.any? ? mentions.last.tweet_id : 1 
    #puts last_mention_id #todo: remove it
    options = {
      since_id: last_mention_id,
      count: 	200
    }
    CLIENT.mentions_timeline(options)
  end

  def self.to_all_mentions
  	if mentions = check_mentions
  	  for tweet in mentions do
  	  	mention = Mention.new(tweet_id: tweet.id)
  	  	reply = generate_reply_to(tweet) + " " + Emoji.random
        if CLIENT.update("@#{tweet.user.screen_name} " + reply, in_reply_to_status_id: tweet.id)
          mention.save
        end
      end
    end
  end
end